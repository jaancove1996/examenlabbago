const express = require('express');
const mysql = require('mysql');

const bodyParser = require('body-parser');

const PORT = process.env.PORT || 3050;

const app = express();

app.use(bodyParser.json());

//my sql
const connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : '',
    database : 'laboratorios_bago'
  });

  //rutas
app.get('/', (req,res) =>{
    res.send('Bienvenido a mi api! ');
});

//todos los datos
app.get('/datos', (req,res) =>{
    //res.send('Lista de la tabla datos ');
    const sql = 'SELECT * FROM datos';

    connection.query(sql, (error, results) =>{
        if (error) throw error;
        if (results.length > 0){
            res.json(results);
        } else {
            res.send('No se han encontrado resultados');
        } 
    });

});

//todos los registros de vacunas
app.get('/vacunas', (req,res) =>{
    //res.send('Lista de la tabla datos ');
    const sql = 'SELECT * FROM vacunas';

    connection.query(sql, (error, results) =>{
        if (error) throw error;
        if (results.length > 0){
            res.json(results);
        } else {
            res.send('No se han encontrado vacunas registradas');
        } 
    });

});

//todos los registros de PROVINCIAS Y CANTONES
app.get('/provincias', (req,res) =>{
    //res.send('Lista de la tabla datos ');
    const sql = 'SELECT * FROM provincia_ciudad';

    connection.query(sql, (error, results) =>{
        if (error) throw error;
        if (results.length > 0){
            res.json(results);
        } else {
            res.send('No se han encontrado vacunas registradas');
        } 
    });

});

//todas las personas registradas
app.get('/getregistrosv', (req,res) =>{
    //res.send('Lista de la tabla datos ');
    const sql = 'SELECT * FROM personas_registro';

    connection.query(sql, (error, results) =>{
        if (error) throw error;
        if (results.length > 0){
            res.json(results);
        } else {
            res.send('No se han encontrado vacunas registradas');
        } 
    });

});

app.get('/datos/:id', (req,res) =>{
    //res.send('Lista por id de la tabla datos ');
    const {id} = req.params;
    const sql = `SELECT * FROM DATOS WHERE ID = '${id}'`;

    connection.query(sql, (error, result) =>{
        if (error) throw error;
        if (result.length > 0){
            res.json(result);
        } else {
            res.send('No se han encontrado resultados para el id ');
        } 
    });
});

//ver el registro de una persona
app.get('/getregistrosv/:id', (req,res) =>{
    //res.send('Lista por id de la tabla datos ');
    const {id} = req.params;
    const sql = `SELECT * FROM PERSONAS_REGISTRO WHERE ID_PERSONA = '${id}'`;

    connection.query(sql, (error, result) =>{
        if (error) throw error;
        if (result.length > 0){
            res.json(result);
        } else {
            res.send('No se han encontrado resultados para el id ');
        } 
    });
});

//add prueba
app.post('/add', (req,res) =>{
    //res.send('Insert Dato ');
    const sql = 'INSERT INTO DATOS SET ?';

    const datosObj ={
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        cedula: req.body.cedula
    }

    connection.query(sql, datosObj, error =>{
        if (error) throw error;
        res.send('Dato Insertado!');
    });
});

//add vacuna
app.post('/vacuna/add', (req,res) =>{
    //res.send('Insert Dato ');
    const sql = 'INSERT INTO VACUNAS SET ?';

    const datosObj ={
        nombre_vacuna: req.body.nombre_vacuna,
        fabricante: req.body.fabricante,
        fecha_registro: req.body.fecha_registro
    }

    connection.query(sql, datosObj, error =>{
        if (error) throw error;
        res.send('Vacuna Insertada Correctamente!');
    });
});

//add detalle vacuna
app.post('/detallevacuna/add', (req,res) =>{
    //res.send('Insert Dato ');
    const sql = 'INSERT INTO DETALLE_VACUNA SET ?';

    const datosObj ={
        lote: req.body.lote,
        cantidad_recibidas: req.body.cantidad_recibidas,
        stock: req.body.stock,
        fecha_llegada: req.body.fecha_llegada,
        dias_dosis: req.body.dias_dosis,
        cantidad_dosis: req.body.cantidad_dosis,
        id_vacuna: req.body.id_vacuna
    }

    connection.query(sql, datosObj, error =>{
        if (error) throw error;
        res.send('Detalle de la Vacuna Insertada Correctamente!');
    });
});

//add registro de vacunacion
app.post('/registrovacuna/add', (req,res) =>{
    //res.send('Insert Dato ');
    const sql = 'INSERT INTO PERSONAS_REGISTRO SET ?';

    const datosObj ={
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        cedula: req.body.cedula,
        fecha_nacimiento: req.body.fecha_nacimiento,
        id_provincia_ciudad: req.body.id_provincia_ciudad,
        lugar_vacunacion: req.body.lugar_vacunacion,
        id_vacuna: req.body.id_vacuna,
        fecha_dosis_1: req.body.fecha_dosis_1,
        fecha_dosis_2: req.body.fecha_dosis_2,
        fecha_dosis_3: req.body.fecha_dosis_3,
        observacion: req.body.observacion,
        foto: req.body.foto,
        fecha_registro: req.body.fecha_registro

    }

    connection.query(sql, datosObj, error =>{
        if (error) throw error;
        res.send('Registro de Vacunacion Creado Correctamente!');
    });
});

//add provincia_ciudad
app.post('/provinciaciudad/add', (req,res) =>{
    //res.send('Insert Dato ');
    const sql = 'INSERT INTO PROVINCIA_CIUDAD SET ?';

    const datosObj ={
        provincia: req.body.provincia,
        ciudad: req.body.ciudad
    }

    connection.query(sql, datosObj, error =>{
        if (error) throw error;
        res.send('Provincia y Ciudad creadas correctamente!');
    });
});

//actualizar id
app.put('/update/:id', (req,res) =>{
    //res.send('Update Dato ');
    const {id} = req.params;
    const {nombres, apellidos, cedula} = req.body;
    const sql =`update datos set nombres= '${nombres}', apellidos = '${apellidos}', cedula= '${cedula}' where id = ${id}`;

    connection.query(sql, error =>{
        if (error) throw error;
        res.send('Dato Actualizado!');
    });
});

//actualizar id persona registro
app.put('/updateregistro/:id', (req,res) =>{
    //res.send('Update Dato ');
    const {id} = req.params;
    const {id_provincia_ciudad, id_vacuna, fecha_dosis_1, fecha_dosis_2, fecha_dosis_3, observacion} = req.body;
    const sql =`update personas_registro set id_provincia_ciudad= '${id_provincia_ciudad}', id_vacuna = '${id_vacuna}', fecha_dosis_1= '${fecha_dosis_1}', fecha_dosis_2= '${fecha_dosis_2}', fecha_dosis_3='${fecha_dosis_3}' , observacion = '${observacion}' where id_persona = ${id}`;
    //const sql =`update personas_registro p set p.id_provincia_ciudad= IFNULL('${id_provincia_ciudad}',p.id_provincia_ciudad), p.id_vacuna = IFNULL('${id_vacuna}',p.id_vacuna), p.fecha_dosis_1= IFNULL('${fecha_dosis_1}',p.fecha_dosis_1), p.fecha_dosis_2= IFNULL('${fecha_dosis_2}',p.fecha_dosis_2), p.fecha_dosis_3=IFNULL('${fecha_dosis_3}',p.fecha_dosis_3), p.observacion = IFNULL('${observacion}',p.observacion) where id_persona = ${id}`;

    connection.query(sql, error =>{
        if (error) throw error;
        res.send('Dato Actualizado!');
    });
});

app.delete('/delete/:id', (req,res) =>{
    //res.send('delete Dato ');
    const {id} = req.params;
    sql = `DELETE FROM DATOS WHERE ID= '${id}'`;

    connection.query(sql, error =>{
        if (error) throw error;
        res.send('Deleted dato!');
    });
});

//eliminar persona registrada
app.delete('/deletepersona/:id', (req,res) =>{
    //res.send('delete Dato ');
    const {id} = req.params;
    sql = `DELETE FROM personas_registro WHERE ID_PERSONA= '${id}'`;

    connection.query(sql, error =>{
        if (error) throw error;
        res.send('Deleted dato!');
    });
});

  //check conection
connection.connect(error =>{
    if (error) throw error;
    console.log('Conexion Exitosa!');
});

app.listen(PORT, ()=>console.log(`Servicio ejecutandose en el puerto '${PORT}'`));


